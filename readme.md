
# Getting started

- Install [stack](https://docs.haskellstack.org/en/stable/install_and_upgrade/)
- `stack setup` (downloads Haskell compiler)
- `stack repl` for development
- `main` to run the game, `:r` to reload changes
- [Search engine for types and APIs](https://www.stackage.org/lts-7.19)
