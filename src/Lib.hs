
module Lib where

import Control.Monad.State
import Control.Monad.Except
import Control.Monad.Random hiding (fromList)
import System.Random.Shuffle
import Data.List.NonEmpty (NonEmpty((:|)), nonEmpty, fromList, toList, uncons)

-- | Cards are parameterised by user-defined state
data Card c = Card c
  deriving (Show, Eq)

-- | Decks are guaranteed to be non-empty
type Deck c = NonEmpty (Card c)

-- | The context of games. Games have state, can perform IO, and are
-- | parameterised by some game state g. a is some value computed by
-- | functions acting in the context of games.
type Game g a = StateT g IO a

-- | Shuffles a deck in the context of a game (randomness requires IO).
shuffle :: Deck c -> Game g (Deck c)
shuffle deck = liftIO (shuffleMNE deck)
  where
    -- | shuffleM for non-empty lists
    shuffleMNE :: MonadRandom m => NonEmpty a -> m (NonEmpty a)
    shuffleMNE = fmap fromList . shuffleM . toList

-- | Draws from a deck in the context of a game. The second argument
-- | is a means of refilling the deck when it becomes empty.
draw :: Deck c -> Game g (Deck c) -> Game g (Card c, Deck c)
draw deck refill = do
  let (h, t) = uncons deck
  d <- case t of
    Just d -> return d
    Nothing -> refill
  return (h, d)

-- | Produce a deck filled with n of a particular card
fillN :: Int -> Card c -> Game g (Deck c)
fillN n card = return (fromList (replicate n card))
