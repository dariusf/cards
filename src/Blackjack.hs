
module Blackjack where

import Lib
import Data.List.NonEmpty (fromList, toList)
import Control.Monad.State hiding (State)

-- This isn't really blackjack

data BCard
  = Ace
  | Queen
  | Two
  deriving (Show, Eq)

type BDeck = Deck BCard

type State = BDeck

deck :: BDeck
deck = fromList (map Card [Ace, Two, Queen])

refill :: Game State BDeck
refill = return deck

-- | The game is just to draw random cards from the deck.
play :: Int -> Game State ()
play 0 = liftIO (putStrLn "Game ended")
play n = do
  deck <- shuffle =<< get
  (card, deck') <- draw deck refill
  put deck'
  liftIO (putStrLn ("Drew " ++ show card))
  play (n - 1)

start :: IO ()
start = do
  (_, finalDeck) <- runStateT (play 5) deck
  print (toList finalDeck)
